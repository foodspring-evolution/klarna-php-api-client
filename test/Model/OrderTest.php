<?php
/**
 * OrderTest
 *
 * PHP version 5
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Klarna Payments API V1
 *
 * API spec
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.27
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Please update the test case below to test the model.
 */

namespace Foodspring\Klarna;

/**
 * OrderTest Class Doc Comment
 *
 * @category    Class
 * @description Order
 * @package     Foodspring\Klarna
 * @author      Swagger Codegen team
 * @link        https://github.com/swagger-api/swagger-codegen
 */
class OrderTest extends \PHPUnit_Framework_TestCase
{

    /**
     * Setup before running any test case
     */
    public static function setUpBeforeClass()
    {
    }

    /**
     * Setup before running each test case
     */
    public function setUp()
    {
    }

    /**
     * Clean up after running each test case
     */
    public function tearDown()
    {
    }

    /**
     * Clean up after running all test cases
     */
    public static function tearDownAfterClass()
    {
    }

    /**
     * Test "Order"
     */
    public function testOrder()
    {
    }

    /**
     * Test attribute "authorizedPaymentMethod"
     */
    public function testPropertyAuthorizedPaymentMethod()
    {
    }

    /**
     * Test attribute "fraudStatus"
     */
    public function testPropertyFraudStatus()
    {
    }

    /**
     * Test attribute "orderId"
     */
    public function testPropertyOrderId()
    {
    }

    /**
     * Test attribute "redirectUrl"
     */
    public function testPropertyRedirectUrl()
    {
    }
}

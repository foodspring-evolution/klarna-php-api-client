.PHONY: default
default: help ;

.PHONY: generate
generate: #? Regenerate client code from Swagger specs contained in swagger/ directory
	docker run --rm -v ${PWD}:/local swaggerapi/swagger-codegen-cli-v3 generate \
        -i /local/swagger/payments.json \
        -l php \
        -o /local/ \
        -c /local/codegen-config.json

.PHONY: help
help: #? Display this help
	$(info Available targets)
	@awk '/^@?[a-zA-Z\-\_0-9]+:/ { \
		nb = sub( /^#\? /, "", helpMsg ); \
		if(nb == 0) { \
			helpMsg = $$0; \
			nb = sub( /^[^:]*:.* #\? /, "", helpMsg ); \
		} \
		if (nb) \
			printf "\033[1;31m%-" width "s\033[0m %s\n", $$1, helpMsg; \
	} \
	{ helpMsg = $$0 }' \
	$(MAKEFILE_LIST) | column -ts:

# Options

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**colorBorder** | **string** | Color for the border of elements within the iFrame. Value should be a CSS hex color, e.g. \&quot;#FF9900\&quot; | [optional] 
**colorBorderSelected** | **string** | Color for the border of elements within the iFrame when selected by the customer. Value should be a CSS hex color, e.g. \&quot;#FF9900\&quot; | [optional] 
**colorDetails** | **string** | Color for the bullet points within the iFrame. Value should be a CSS hex color, e.g. \&quot;#FF9900\&quot; | [optional] 
**colorText** | **string** | Color for the texts within the iFrame. Value should be a CSS hex color, e.g. \&quot;#FF9900\&quot; | [optional] 
**radiusBorder** | **string** | Radius for the border of elements within the iFrame. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


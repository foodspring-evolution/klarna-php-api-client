# MerchantSession

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clientToken** | **string** | Client token to be passed to the JS client while initializing the JS SDK in the next step. | 
**paymentMethodCategories** | [**\Foodspring\Klarna\Model\PaymentMethodCategory[]**](PaymentMethodCategory.md) | Available payment method categories for this particular session | [optional] 
**sessionId** | **string** | ID of the created session. Please use this ID to share with Klarna for identifying any issues during integration. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


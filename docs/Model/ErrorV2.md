# ErrorV2

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authorizedPaymentMethod** | [**\Foodspring\Klarna\Model\AuthorizedPaymentMethod**](AuthorizedPaymentMethod.md) |  | [optional] 
**correlationId** | **string** |  | [optional] 
**errorCode** | **string** |  | [optional] 
**errorMessages** | **string[]** |  | [optional] 
**fraudStatus** | **string** |  | [optional] 
**reason** | **string** |  | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


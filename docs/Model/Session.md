# Session

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acquiringChannel** | **string** | The acquiring channel in which the session takes place. Ecommerce is default unless specified. Any other values should be defined in the agreement. | [optional] 
**attachment** | [**\Foodspring\Klarna\Model\Attachment**](Attachment.md) |  | [optional] 
**authorizationToken** | **string** | Authorization token. | [optional] 
**billingAddress** | [**\Foodspring\Klarna\Model\Address**](Address.md) |  | [optional] 
**clientToken** | **string** | Token to be passed to the JS client | [optional] 
**customPaymentMethodIds** | **string[]** | Promo codes - The array could be used to define which of the configured payment options within a payment category (pay_later, pay_over_time, etc.) should be shown for this purchase. Discuss with the delivery manager to know about the promo codes that will be configured for your account. The feature could also be used to provide promotional offers to specific customers (eg: 0% financing). Please be informed that the usage of this feature can have commercial implications. | [optional] 
**customer** | [**\Foodspring\Klarna\Model\Customer**](Customer.md) |  | [optional] 
**design** | **string** | Design package to use in the session. This can only by used if a custom design has been implemented for Klarna Payments and agreed upon in the agreement. It might have a financial impact. Delivery manager will provide the value for the parameter. | [optional] 
**expiresAt** | [**\Foodspring\Klarna\Model\Instant**](Instant.md) |  | [optional] 
**locale** | **string** | Used to define the language and region of the customer. The locale follows the format of RFC 1766, meaning language-country The following values are applicable:  AT: \&quot;de-AT\&quot;, \&quot;de-DE\&quot;, \&quot;en-DE\&quot; BE: \&quot;be-BE\&quot;, \&quot;nl-BE\&quot;, \&quot;fr-BE\&quot;, \&quot;en-BE\&quot; CH: \&quot;it-CH\&quot;, \&quot;de-CH\&quot;, \&quot;fr-CH\&quot;, \&quot;en-CH\&quot; DE: \&quot;de-DE\&quot;, \&quot;de-AT\&quot;, \&quot;en-DE\&quot; DK: \&quot;da-DK\&quot;, \&quot;en-DK\&quot; ES: \&quot;es-ES\&quot;, \&quot;ca-ES\&quot;, \&quot;en-ES\&quot; FI: \&quot;fi-FI\&quot;, \&quot;sv-FI\&quot;, \&quot;en-FI\&quot; GB: \&quot;en-GB\&quot; IT: \&quot;it-IT\&quot;, \&quot;en-IT\&quot; NL: \&quot;nl-NL\&quot;, \&quot;en-NL\&quot; NO: \&quot;nb-NO\&quot;, \&quot;en-NO\&quot; PL: \&quot;pl-PL\&quot;, \&quot;en-PL\&quot; SE: \&quot;sv-SE\&quot;, \&quot;en-SE\&quot; US: \&quot;en-US\&quot; | 
**merchantData** | **string** | Pass through field to send any information about the order to be used later for reference while retrieving the order details (max 1024 characters) | [optional] 
**merchantReference1** | **string** | Used for storing merchant&#x27;s internal order number or other reference. If set, will be shown on the confirmation page as \&quot;order number\&quot; and send to the customer in the confirmation mail after a successful direct bank transfer payment. It will also be included in the payments description in the customer&#x27;s bank account and settlement files to the merchant (max 255 characters). | [optional] 
**merchantReference2** | **string** | Used for storing merchant&#x27;s internal order number or other reference. The value is available in the settlement files. (max 255 characters). | [optional] 
**merchantUrls** | [**\Foodspring\Klarna\Model\MerchantUrls**](MerchantUrls.md) |  | [optional] 
**options** | [**\Foodspring\Klarna\Model\Options**](Options.md) |  | [optional] 
**orderAmount** | **int** | Total amount of the order including tax and any available discounts. The value should be in non-negative minor units. Eg: 25 Euros should be 2500. | 
**orderLines** | [**\Foodspring\Klarna\Model\OrderLine[]**](OrderLine.md) | The array containing list of line items that are part of this order. Maximum of 1000 line items could be processed in a single order. | 
**orderTaxAmount** | **int** | Total tax amount of the order. The value should be in non-negative minor units. Eg: 25 Euros should be 2500. | [optional] 
**paymentMethodCategories** | [**\Foodspring\Klarna\Model\PaymentMethodCategory[]**](PaymentMethodCategory.md) | Available payment method categories | [optional] 
**purchaseCountry** | **string** | The purchase country of the customer. The billing country always overrides purchase country if the values are different. Formatted according to ISO 3166 alpha-2 standard, e.g. GB, SE, DE, US, etc. | 
**purchaseCurrency** | **string** | The purchase currency of the order. Formatted according to ISO 4217 standard, e.g. USD, EUR, SEK, GBP, etc. | 
**shippingAddress** | [**\Foodspring\Klarna\Model\Address**](Address.md) |  | [optional] 
**status** | **string** | The current status of the session. Possible values: &#x27;complete&#x27;, &#x27;incomplete&#x27; where &#x27;complete&#x27; is set when the order has been placed. | [optional] 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


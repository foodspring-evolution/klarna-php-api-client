# CustomerTokenCreationRequest

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**billingAddress** | [**\Foodspring\Klarna\Model\Address**](Address.md) |  | [optional] 
**customer** | [**\Foodspring\Klarna\Model\Customer**](Customer.md) |  | [optional] 
**description** | **string** | Description of the purpose of the token. | 
**intendedUse** | **string** | Intended use for the token. | 
**locale** | **string** | RFC 1766 customer&#x27;s locale. | 
**purchaseCountry** | **string** | ISO 3166 alpha-2 purchase country. | 
**purchaseCurrency** | **string** | ISO 4217 purchase currency. | 

[[Back to Model list]](../../README.md#documentation-for-models) [[Back to API list]](../../README.md#documentation-for-api-endpoints) [[Back to README]](../../README.md)


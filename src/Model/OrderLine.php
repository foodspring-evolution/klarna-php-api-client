<?php
/**
 * OrderLine
 *
 * PHP version 5
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Klarna Payments API V1
 *
 * API spec
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.27
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Foodspring\Klarna\Model;

use \ArrayAccess;
use \Foodspring\Klarna\ObjectSerializer;

/**
 * OrderLine Class Doc Comment
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class OrderLine implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'order_line';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'imageUrl' => 'string',
'merchantData' => 'string',
'name' => 'string',
'productIdentifiers' => '\Foodspring\Klarna\Model\ProductIdentifiers',
'productUrl' => 'string',
'quantity' => 'int',
'quantityUnit' => 'string',
'reference' => 'string',
'taxRate' => 'int',
'totalAmount' => 'int',
'totalDiscountAmount' => 'int',
'totalTaxAmount' => 'int',
'type' => 'string',
'unitPrice' => 'int'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'imageUrl' => null,
'merchantData' => null,
'name' => null,
'productIdentifiers' => null,
'productUrl' => null,
'quantity' => 'int64',
'quantityUnit' => null,
'reference' => null,
'taxRate' => 'int64',
'totalAmount' => 'int64',
'totalDiscountAmount' => 'int64',
'totalTaxAmount' => 'int64',
'type' => null,
'unitPrice' => 'int64'    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'imageUrl' => 'image_url',
'merchantData' => 'merchant_data',
'name' => 'name',
'productIdentifiers' => 'product_identifiers',
'productUrl' => 'product_url',
'quantity' => 'quantity',
'quantityUnit' => 'quantity_unit',
'reference' => 'reference',
'taxRate' => 'tax_rate',
'totalAmount' => 'total_amount',
'totalDiscountAmount' => 'total_discount_amount',
'totalTaxAmount' => 'total_tax_amount',
'type' => 'type',
'unitPrice' => 'unit_price'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'imageUrl' => 'setImageUrl',
'merchantData' => 'setMerchantData',
'name' => 'setName',
'productIdentifiers' => 'setProductIdentifiers',
'productUrl' => 'setProductUrl',
'quantity' => 'setQuantity',
'quantityUnit' => 'setQuantityUnit',
'reference' => 'setReference',
'taxRate' => 'setTaxRate',
'totalAmount' => 'setTotalAmount',
'totalDiscountAmount' => 'setTotalDiscountAmount',
'totalTaxAmount' => 'setTotalTaxAmount',
'type' => 'setType',
'unitPrice' => 'setUnitPrice'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'imageUrl' => 'getImageUrl',
'merchantData' => 'getMerchantData',
'name' => 'getName',
'productIdentifiers' => 'getProductIdentifiers',
'productUrl' => 'getProductUrl',
'quantity' => 'getQuantity',
'quantityUnit' => 'getQuantityUnit',
'reference' => 'getReference',
'taxRate' => 'getTaxRate',
'totalAmount' => 'getTotalAmount',
'totalDiscountAmount' => 'getTotalDiscountAmount',
'totalTaxAmount' => 'getTotalTaxAmount',
'type' => 'getType',
'unitPrice' => 'getUnitPrice'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['imageUrl'] = isset($data['imageUrl']) ? $data['imageUrl'] : null;
        $this->container['merchantData'] = isset($data['merchantData']) ? $data['merchantData'] : null;
        $this->container['name'] = isset($data['name']) ? $data['name'] : null;
        $this->container['productIdentifiers'] = isset($data['productIdentifiers']) ? $data['productIdentifiers'] : null;
        $this->container['productUrl'] = isset($data['productUrl']) ? $data['productUrl'] : null;
        $this->container['quantity'] = isset($data['quantity']) ? $data['quantity'] : null;
        $this->container['quantityUnit'] = isset($data['quantityUnit']) ? $data['quantityUnit'] : null;
        $this->container['reference'] = isset($data['reference']) ? $data['reference'] : null;
        $this->container['taxRate'] = isset($data['taxRate']) ? $data['taxRate'] : null;
        $this->container['totalAmount'] = isset($data['totalAmount']) ? $data['totalAmount'] : null;
        $this->container['totalDiscountAmount'] = isset($data['totalDiscountAmount']) ? $data['totalDiscountAmount'] : null;
        $this->container['totalTaxAmount'] = isset($data['totalTaxAmount']) ? $data['totalTaxAmount'] : null;
        $this->container['type'] = isset($data['type']) ? $data['type'] : null;
        $this->container['unitPrice'] = isset($data['unitPrice']) ? $data['unitPrice'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        if ($this->container['name'] === null) {
            $invalidProperties[] = "'name' can't be null";
        }
        if ($this->container['quantity'] === null) {
            $invalidProperties[] = "'quantity' can't be null";
        }
        if ($this->container['totalAmount'] === null) {
            $invalidProperties[] = "'totalAmount' can't be null";
        }
        if ($this->container['unitPrice'] === null) {
            $invalidProperties[] = "'unitPrice' can't be null";
        }
        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets imageUrl
     *
     * @return string
     */
    public function getImageUrl()
    {
        return $this->container['imageUrl'];
    }

    /**
     * Sets imageUrl
     *
     * @param string $imageUrl URL to an image that can be later embedded in communications between Klarna and the customer. (max 1024 characters).  A minimum of 250x250 px resolution is recommended for the image to look good in the Klarna app, and below 50x50 px won't even show. We recommend using a good sized image (650x650 px or more), however the file size must not exceed 12MB.
     *
     * @return $this
     */
    public function setImageUrl($imageUrl)
    {
        $this->container['imageUrl'] = $imageUrl;

        return $this;
    }

    /**
     * Gets merchantData
     *
     * @return string
     */
    public function getMerchantData()
    {
        return $this->container['merchantData'];
    }

    /**
     * Sets merchantData
     *
     * @param string $merchantData Used for storing merchant's internal order number or other reference. Pass through field. (max 255 characters)
     *
     * @return $this
     */
    public function setMerchantData($merchantData)
    {
        $this->container['merchantData'] = $merchantData;

        return $this;
    }

    /**
     * Gets name
     *
     * @return string
     */
    public function getName()
    {
        return $this->container['name'];
    }

    /**
     * Sets name
     *
     * @param string $name Descriptive name of the order line item.
     *
     * @return $this
     */
    public function setName($name)
    {
        $this->container['name'] = $name;

        return $this;
    }

    /**
     * Gets productIdentifiers
     *
     * @return \Foodspring\Klarna\Model\ProductIdentifiers
     */
    public function getProductIdentifiers()
    {
        return $this->container['productIdentifiers'];
    }

    /**
     * Sets productIdentifiers
     *
     * @param \Foodspring\Klarna\Model\ProductIdentifiers $productIdentifiers productIdentifiers
     *
     * @return $this
     */
    public function setProductIdentifiers($productIdentifiers)
    {
        $this->container['productIdentifiers'] = $productIdentifiers;

        return $this;
    }

    /**
     * Gets productUrl
     *
     * @return string
     */
    public function getProductUrl()
    {
        return $this->container['productUrl'];
    }

    /**
     * Sets productUrl
     *
     * @param string $productUrl URL to the product in the merchant’s webshop that can be later used in communications between Klarna and the customer. (max 1024 characters)
     *
     * @return $this
     */
    public function setProductUrl($productUrl)
    {
        $this->container['productUrl'] = $productUrl;

        return $this;
    }

    /**
     * Gets quantity
     *
     * @return int
     */
    public function getQuantity()
    {
        return $this->container['quantity'];
    }

    /**
     * Sets quantity
     *
     * @param int $quantity Quantity of the order line item. Must be a non-negative number.
     *
     * @return $this
     */
    public function setQuantity($quantity)
    {
        $this->container['quantity'] = $quantity;

        return $this;
    }

    /**
     * Gets quantityUnit
     *
     * @return string
     */
    public function getQuantityUnit()
    {
        return $this->container['quantityUnit'];
    }

    /**
     * Sets quantityUnit
     *
     * @param string $quantityUnit Unit used to describe the quantity, e.g. kg, pcs, etc. If defined the value has to be 1-8 characters.
     *
     * @return $this
     */
    public function setQuantityUnit($quantityUnit)
    {
        $this->container['quantityUnit'] = $quantityUnit;

        return $this;
    }

    /**
     * Gets reference
     *
     * @return string
     */
    public function getReference()
    {
        return $this->container['reference'];
    }

    /**
     * Sets reference
     *
     * @param string $reference Client facing article number, SKU or similar. Max length is 64 characters.
     *
     * @return $this
     */
    public function setReference($reference)
    {
        $this->container['reference'] = $reference;

        return $this;
    }

    /**
     * Gets taxRate
     *
     * @return int
     */
    public function getTaxRate()
    {
        return $this->container['taxRate'];
    }

    /**
     * Sets taxRate
     *
     * @param int $taxRate Tax rate of the order line. Non-negative value. The percentage value is represented with two implicit decimals. I.e 1900 = 19%.
     *
     * @return $this
     */
    public function setTaxRate($taxRate)
    {
        $this->container['taxRate'] = $taxRate;

        return $this;
    }

    /**
     * Gets totalAmount
     *
     * @return int
     */
    public function getTotalAmount()
    {
        return $this->container['totalAmount'];
    }

    /**
     * Sets totalAmount
     *
     * @param int $totalAmount Total amount of the order line. Must be defined as non-negative minor units. Includes tax and discount. Eg: 2500=25 euros Value = (quantity x unit_price) - total_discount_amount.  (max value: 100000000)
     *
     * @return $this
     */
    public function setTotalAmount($totalAmount)
    {
        $this->container['totalAmount'] = $totalAmount;

        return $this;
    }

    /**
     * Gets totalDiscountAmount
     *
     * @return int
     */
    public function getTotalDiscountAmount()
    {
        return $this->container['totalDiscountAmount'];
    }

    /**
     * Sets totalDiscountAmount
     *
     * @param int $totalDiscountAmount Non-negative minor units. Includes tax. Eg: 500=5 euros
     *
     * @return $this
     */
    public function setTotalDiscountAmount($totalDiscountAmount)
    {
        $this->container['totalDiscountAmount'] = $totalDiscountAmount;

        return $this;
    }

    /**
     * Gets totalTaxAmount
     *
     * @return int
     */
    public function getTotalTaxAmount()
    {
        return $this->container['totalTaxAmount'];
    }

    /**
     * Sets totalTaxAmount
     *
     * @param int $totalTaxAmount Total tax amount of the order line. Must be within ±1 of total_amount - total_amount 10000 / (10000 + tax_rate). Negative when type is discount.
     *
     * @return $this
     */
    public function setTotalTaxAmount($totalTaxAmount)
    {
        $this->container['totalTaxAmount'] = $totalTaxAmount;

        return $this;
    }

    /**
     * Gets type
     *
     * @return string
     */
    public function getType()
    {
        return $this->container['type'];
    }

    /**
     * Sets type
     *
     * @param string $type Type of the order line item. The possible values are:  physical discount shipping_fee sales_tax digital gift_card store_credit surcharge
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->container['type'] = $type;

        return $this;
    }

    /**
     * Gets unitPrice
     *
     * @return int
     */
    public function getUnitPrice()
    {
        return $this->container['unitPrice'];
    }

    /**
     * Sets unitPrice
     *
     * @param int $unitPrice Price for a single unit of the order line. Non-negative minor units. Includes tax, excludes discount. (max value: 100000000)
     *
     * @return $this
     */
    public function setUnitPrice($unitPrice)
    {
        $this->container['unitPrice'] = $unitPrice;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}

<?php
/**
 * Options
 *
 * PHP version 5
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Klarna Payments API V1
 *
 * API spec
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.27
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Foodspring\Klarna\Model;

use \ArrayAccess;
use \Foodspring\Klarna\ObjectSerializer;

/**
 * Options Class Doc Comment
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class Options implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'options';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'colorBorder' => 'string',
'colorBorderSelected' => 'string',
'colorDetails' => 'string',
'colorText' => 'string',
'radiusBorder' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'colorBorder' => null,
'colorBorderSelected' => null,
'colorDetails' => null,
'colorText' => null,
'radiusBorder' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'colorBorder' => 'color_border',
'colorBorderSelected' => 'color_border_selected',
'colorDetails' => 'color_details',
'colorText' => 'color_text',
'radiusBorder' => 'radius_border'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'colorBorder' => 'setColorBorder',
'colorBorderSelected' => 'setColorBorderSelected',
'colorDetails' => 'setColorDetails',
'colorText' => 'setColorText',
'radiusBorder' => 'setRadiusBorder'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'colorBorder' => 'getColorBorder',
'colorBorderSelected' => 'getColorBorderSelected',
'colorDetails' => 'getColorDetails',
'colorText' => 'getColorText',
'radiusBorder' => 'getRadiusBorder'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['colorBorder'] = isset($data['colorBorder']) ? $data['colorBorder'] : null;
        $this->container['colorBorderSelected'] = isset($data['colorBorderSelected']) ? $data['colorBorderSelected'] : null;
        $this->container['colorDetails'] = isset($data['colorDetails']) ? $data['colorDetails'] : null;
        $this->container['colorText'] = isset($data['colorText']) ? $data['colorText'] : null;
        $this->container['radiusBorder'] = isset($data['radiusBorder']) ? $data['radiusBorder'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets colorBorder
     *
     * @return string
     */
    public function getColorBorder()
    {
        return $this->container['colorBorder'];
    }

    /**
     * Sets colorBorder
     *
     * @param string $colorBorder Color for the border of elements within the iFrame. Value should be a CSS hex color, e.g. \"#FF9900\"
     *
     * @return $this
     */
    public function setColorBorder($colorBorder)
    {
        $this->container['colorBorder'] = $colorBorder;

        return $this;
    }

    /**
     * Gets colorBorderSelected
     *
     * @return string
     */
    public function getColorBorderSelected()
    {
        return $this->container['colorBorderSelected'];
    }

    /**
     * Sets colorBorderSelected
     *
     * @param string $colorBorderSelected Color for the border of elements within the iFrame when selected by the customer. Value should be a CSS hex color, e.g. \"#FF9900\"
     *
     * @return $this
     */
    public function setColorBorderSelected($colorBorderSelected)
    {
        $this->container['colorBorderSelected'] = $colorBorderSelected;

        return $this;
    }

    /**
     * Gets colorDetails
     *
     * @return string
     */
    public function getColorDetails()
    {
        return $this->container['colorDetails'];
    }

    /**
     * Sets colorDetails
     *
     * @param string $colorDetails Color for the bullet points within the iFrame. Value should be a CSS hex color, e.g. \"#FF9900\"
     *
     * @return $this
     */
    public function setColorDetails($colorDetails)
    {
        $this->container['colorDetails'] = $colorDetails;

        return $this;
    }

    /**
     * Gets colorText
     *
     * @return string
     */
    public function getColorText()
    {
        return $this->container['colorText'];
    }

    /**
     * Sets colorText
     *
     * @param string $colorText Color for the texts within the iFrame. Value should be a CSS hex color, e.g. \"#FF9900\"
     *
     * @return $this
     */
    public function setColorText($colorText)
    {
        $this->container['colorText'] = $colorText;

        return $this;
    }

    /**
     * Gets radiusBorder
     *
     * @return string
     */
    public function getRadiusBorder()
    {
        return $this->container['radiusBorder'];
    }

    /**
     * Sets radiusBorder
     *
     * @param string $radiusBorder Radius for the border of elements within the iFrame.
     *
     * @return $this
     */
    public function setRadiusBorder($radiusBorder)
    {
        $this->container['radiusBorder'] = $radiusBorder;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}

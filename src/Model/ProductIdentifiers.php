<?php
/**
 * ProductIdentifiers
 *
 * PHP version 5
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */

/**
 * Klarna Payments API V1
 *
 * API spec
 *
 * OpenAPI spec version: 1.0.0
 * 
 * Generated by: https://github.com/swagger-api/swagger-codegen.git
 * Swagger Codegen version: 3.0.27
 */
/**
 * NOTE: This class is auto generated by the swagger code generator program.
 * https://github.com/swagger-api/swagger-codegen
 * Do not edit the class manually.
 */

namespace Foodspring\Klarna\Model;

use \ArrayAccess;
use \Foodspring\Klarna\ObjectSerializer;

/**
 * ProductIdentifiers Class Doc Comment
 *
 * @category Class
 * @package  Foodspring\Klarna
 * @author   Swagger Codegen team
 * @link     https://github.com/swagger-api/swagger-codegen
 */
class ProductIdentifiers implements ModelInterface, ArrayAccess
{
    const DISCRIMINATOR = null;

    /**
      * The original name of the model.
      *
      * @var string
      */
    protected static $swaggerModelName = 'product_identifiers';

    /**
      * Array of property to type mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerTypes = [
        'brand' => 'string',
'categoryPath' => 'string',
'globalTradeItemNumber' => 'string',
'manufacturerPartNumber' => 'string'    ];

    /**
      * Array of property to format mappings. Used for (de)serialization
      *
      * @var string[]
      */
    protected static $swaggerFormats = [
        'brand' => null,
'categoryPath' => null,
'globalTradeItemNumber' => null,
'manufacturerPartNumber' => null    ];

    /**
     * Array of property to type mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerTypes()
    {
        return self::$swaggerTypes;
    }

    /**
     * Array of property to format mappings. Used for (de)serialization
     *
     * @return array
     */
    public static function swaggerFormats()
    {
        return self::$swaggerFormats;
    }

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @var string[]
     */
    protected static $attributeMap = [
        'brand' => 'brand',
'categoryPath' => 'category_path',
'globalTradeItemNumber' => 'global_trade_item_number',
'manufacturerPartNumber' => 'manufacturer_part_number'    ];

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @var string[]
     */
    protected static $setters = [
        'brand' => 'setBrand',
'categoryPath' => 'setCategoryPath',
'globalTradeItemNumber' => 'setGlobalTradeItemNumber',
'manufacturerPartNumber' => 'setManufacturerPartNumber'    ];

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @var string[]
     */
    protected static $getters = [
        'brand' => 'getBrand',
'categoryPath' => 'getCategoryPath',
'globalTradeItemNumber' => 'getGlobalTradeItemNumber',
'manufacturerPartNumber' => 'getManufacturerPartNumber'    ];

    /**
     * Array of attributes where the key is the local name,
     * and the value is the original name
     *
     * @return array
     */
    public static function attributeMap()
    {
        return self::$attributeMap;
    }

    /**
     * Array of attributes to setter functions (for deserialization of responses)
     *
     * @return array
     */
    public static function setters()
    {
        return self::$setters;
    }

    /**
     * Array of attributes to getter functions (for serialization of requests)
     *
     * @return array
     */
    public static function getters()
    {
        return self::$getters;
    }

    /**
     * The original name of the model.
     *
     * @return string
     */
    public function getModelName()
    {
        return self::$swaggerModelName;
    }

    

    /**
     * Associative array for storing property values
     *
     * @var mixed[]
     */
    protected $container = [];

    /**
     * Constructor
     *
     * @param mixed[] $data Associated array of property values
     *                      initializing the model
     */
    public function __construct(array $data = null)
    {
        $this->container['brand'] = isset($data['brand']) ? $data['brand'] : null;
        $this->container['categoryPath'] = isset($data['categoryPath']) ? $data['categoryPath'] : null;
        $this->container['globalTradeItemNumber'] = isset($data['globalTradeItemNumber']) ? $data['globalTradeItemNumber'] : null;
        $this->container['manufacturerPartNumber'] = isset($data['manufacturerPartNumber']) ? $data['manufacturerPartNumber'] : null;
    }

    /**
     * Show all the invalid properties with reasons.
     *
     * @return array invalid properties with reasons
     */
    public function listInvalidProperties()
    {
        $invalidProperties = [];

        return $invalidProperties;
    }

    /**
     * Validate all the properties in the model
     * return true if all passed
     *
     * @return bool True if all properties are valid
     */
    public function valid()
    {
        return count($this->listInvalidProperties()) === 0;
    }


    /**
     * Gets brand
     *
     * @return string
     */
    public function getBrand()
    {
        return $this->container['brand'];
    }

    /**
     * Sets brand
     *
     * @param string $brand The product's brand name as generally recognized by consumers. If no brand is available for a product, do not supply any value.
     *
     * @return $this
     */
    public function setBrand($brand)
    {
        $this->container['brand'] = $brand;

        return $this;
    }

    /**
     * Gets categoryPath
     *
     * @return string
     */
    public function getCategoryPath()
    {
        return $this->container['categoryPath'];
    }

    /**
     * Sets categoryPath
     *
     * @param string $categoryPath The product's category path as used in the merchant's webshop. Include the full and most detailed category and separate the segments with ' > '
     *
     * @return $this
     */
    public function setCategoryPath($categoryPath)
    {
        $this->container['categoryPath'] = $categoryPath;

        return $this;
    }

    /**
     * Gets globalTradeItemNumber
     *
     * @return string
     */
    public function getGlobalTradeItemNumber()
    {
        return $this->container['globalTradeItemNumber'];
    }

    /**
     * Sets globalTradeItemNumber
     *
     * @param string $globalTradeItemNumber The product's Global Trade Item Number (GTIN). Common types of GTIN are EAN, ISBN or UPC. Exclude dashes and spaces, where possible
     *
     * @return $this
     */
    public function setGlobalTradeItemNumber($globalTradeItemNumber)
    {
        $this->container['globalTradeItemNumber'] = $globalTradeItemNumber;

        return $this;
    }

    /**
     * Gets manufacturerPartNumber
     *
     * @return string
     */
    public function getManufacturerPartNumber()
    {
        return $this->container['manufacturerPartNumber'];
    }

    /**
     * Sets manufacturerPartNumber
     *
     * @param string $manufacturerPartNumber The product's Manufacturer Part Number (MPN), which - together with the brand - uniquely identifies a product. Only submit MPNs assigned by a manufacturer and use the most specific MPN possible
     *
     * @return $this
     */
    public function setManufacturerPartNumber($manufacturerPartNumber)
    {
        $this->container['manufacturerPartNumber'] = $manufacturerPartNumber;

        return $this;
    }
    /**
     * Returns true if offset exists. False otherwise.
     *
     * @param integer $offset Offset
     *
     * @return boolean
     */
    public function offsetExists($offset)
    {
        return isset($this->container[$offset]);
    }

    /**
     * Gets offset.
     *
     * @param integer $offset Offset
     *
     * @return mixed
     */
    public function offsetGet($offset)
    {
        return isset($this->container[$offset]) ? $this->container[$offset] : null;
    }

    /**
     * Sets value based on offset.
     *
     * @param integer $offset Offset
     * @param mixed   $value  Value to be set
     *
     * @return void
     */
    public function offsetSet($offset, $value)
    {
        if (is_null($offset)) {
            $this->container[] = $value;
        } else {
            $this->container[$offset] = $value;
        }
    }

    /**
     * Unsets offset.
     *
     * @param integer $offset Offset
     *
     * @return void
     */
    public function offsetUnset($offset)
    {
        unset($this->container[$offset]);
    }

    /**
     * Gets the string presentation of the object
     *
     * @return string
     */
    public function __toString()
    {
        if (defined('JSON_PRETTY_PRINT')) { // use JSON pretty print
            return json_encode(
                ObjectSerializer::sanitizeForSerialization($this),
                JSON_PRETTY_PRINT
            );
        }

        return json_encode(ObjectSerializer::sanitizeForSerialization($this));
    }
}
